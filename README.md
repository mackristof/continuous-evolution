# Continuous Evolution

> Update your dependencies with (merge | pull)-request.

* [Workflow](#worflow)
* [Development](#development)
* [Contribution](#contribution)
* [Sponsors](#sponsors)

## Workflow

* [ ] User need to give access to ContinuousEvolution
    * direct user (ContinuousEvolution) ?
    * access token ?
    * fork of project with access to user ?
    * ssh public key ?

* [x] download code with git

* [ ] discovery package manager
    * [ ] npm if package.json
    * [ ] maven if pom.xml

* [x] parse depth
    * [x] npm
    * [ ] docker
    * [ ] maven
    * [ ] ...

* [x] retrieve latest versions
    * [x] npm
    * [ ] docker
    * [ ] maven
    * [ ] ...

* [ ] write new depth

* [ ] reporte result
    * [ ] gitlab merge-request
    * [ ] github pull-request
    * [ ] mail
    * [ ] output

## Development

> cd $GOPATH/src

> git clone git@gitlab.com:ContinuousEvolution/continuous-evolution.git

> cd continuous-evolution

> docker-compose up -d

> open http:127.0.0.1:1234/addProject?project=https://github.com/manland/draw-my-project.git&name=draw-my-project

> look at docker-compose logs web to see results

## Contribution

This repository ❤ pull-request ;) Make sure no one else work on same feature by opening an issue!

## Sponsors

