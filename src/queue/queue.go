package queue

import (
	"sync"
	"log"
	"encoding/json"

	"github.com/nsqio/go-nsq"

	"continuous-evolution/src/model"
)

type Queue struct {
	consumer *nsq.Consumer
	producer *nsq.Producer
	waitGroup *sync.WaitGroup
	nsqIp string
}

func NewFacade(nsqIp string, topic string, channel string) (Queue, error) {
	wg := &sync.WaitGroup{}
	wg.Add(1)

	config := nsq.NewConfig()

	q, _ := nsq.NewConsumer(topic, channel, config)
	w, _ := nsq.NewProducer(nsqIp, config)

	queue := Queue {
		consumer: q,
		producer: w,
		waitGroup: wg,
		nsqIp: nsqIp,
	}

	return queue, nil
}

func (q Queue) Handle(handle func(message []byte) error) {
	q.consumer.AddHandler(nsq.HandlerFunc(func (message *nsq.Message) error {
		return handle(message.Body)
	}))
}

func (q Queue) Publish(topic string, body []byte) error {
	return q.producer.Publish(topic, body)
}

func (q Queue) Start(blocking bool) {
	err := q.consumer.ConnectToNSQD(q.nsqIp)
	if err != nil {
		panic(err)
	}
	if blocking {
		q.waitGroup.Wait()
	}
}

func (q Queue) Close() {
	q.producer.Stop()
	q.consumer.DisconnectFromNSQLookupd(q.nsqIp)
	q.waitGroup.Done()
}

/************/
/* SPECIFIC */
/************/

func (q* Queue) HandleProject(handle func(project model.Project) error) {
	q.Handle(func (message []byte) error {
		var project model.Project

		err := json.Unmarshal(message, &project)
		if err != nil {
			log.Panic(err)
			return err
		}

		return handle(project)
	})
}

func (q Queue) PublishProject(topic string, project model.Project) error {
	json, err := json.Marshal(project)
	if err != nil {
		log.Panic(err)
		return err
	}
	return q.Publish(topic, json)
}
