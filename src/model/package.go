package model

import "fmt"

type Package struct {
	Name string `json:"name"`
	PkgVersion string `json:"pkgVersion"`
	ParsedVersion Version `json:"parsedVersion"`
}

func (p Package) String() string {
	return fmt.Sprintf("%s{%s _ %v}", p.Name, p.PkgVersion, p.ParsedVersion)
}
