package model

import "fmt"

type Version struct {
	Symbol string `json:"symbol,omitempty"`
	Major  uint64 `json:"major"`
	Minor  uint64 `json:"minor"`
	Bug    uint64 `json:"bug"`
	Other  string `json:"other,omitempty"`
}

func (from *Version) Equal(other Version) bool {
	return from.Major == other.Major &&
		from.Minor == other.Minor &&
		from.Bug == other.Bug &&
		from.Other == other.Other
}

func (v Version) String() string {
	return fmt.Sprintf("%s%d.%d.%d%s", v.Symbol, v.Major, v.Minor, v.Bug, v.Other)
}
