package model

type Project struct {
	Name string `json:"name"`
	GitUrl string `json:"gitUrl"`
	OriginalFile []byte `json:"originalFile"`
	DepthManager string `json:"depthManager"`
	PathToWrite string `json:"pathToWrite"`
	Packages []Package `json:"packages,omitempty"`
	LatestPackages []Package `json:"latestPackages,omitempty"`
}

func (p Project) AddOriginalFile(originalFile []byte) Project {
	return Project {
		Name: p.Name,
		GitUrl: p.GitUrl,
		OriginalFile: originalFile,
		DepthManager: p.DepthManager,
		PathToWrite: p.PathToWrite,
		Packages: p.Packages,
		LatestPackages: p.LatestPackages,
	}
}

func (p Project) AddPackages(packages []Package) Project {
	return Project {
		Name: p.Name,
		GitUrl: p.GitUrl,
		OriginalFile: p.OriginalFile,
		DepthManager: p.DepthManager,
		PathToWrite: p.PathToWrite,
		Packages: packages,
		LatestPackages: p.LatestPackages,
	}
}

func (p Project) AddLatestPackages(packages []Package) Project {
	return Project {
		Name: p.Name,
		GitUrl: p.GitUrl,
		OriginalFile: p.OriginalFile,
		DepthManager: p.DepthManager,
		PathToWrite: p.PathToWrite,
		Packages: p.Packages,
		LatestPackages:packages,
	}
}
