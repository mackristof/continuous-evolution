package helper

import (
	"regexp"
	"strconv"
	"errors"

	"continuous-evolution/src/model"
	"fmt"
)

var versionRegexp = regexp.MustCompile(`([=|\^|~])?(\d+)\.(\d+)\.(\d+)-?(.*)`)

func NpmStringToVersion(version string) (model.Version, error) {
	parsedVersion := versionRegexp.FindStringSubmatch(version)
	if len(parsedVersion) < 5 {
		return model.Version{}, errors.New(fmt.Sprintf("Version bad formated : %s", version))
	}
	major, err := strconv.ParseUint(parsedVersion[2], 0, 64)
	if err != nil {
		panic(err)
	}
	minor, err := strconv.ParseUint(parsedVersion[3], 0, 64)
	if err != nil {
		panic(err)
	}
	bug, err := strconv.ParseUint(parsedVersion[4], 0, 64)
	if err != nil {
		panic(err)
	}
	return model.Version{
		Symbol: parsedVersion[1],
		Major:  major,
		Minor:  minor,
		Bug:    bug,
		Other:  parsedVersion[5],
	}, nil
}

