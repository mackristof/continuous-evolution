package helper

import (
	"testing"
)

func TestSimple(t *testing.T) {
	parsedVersion, err := NpmStringToVersion("1.1.1")

	if err != nil {
		t.Fatal(err)
	}

	if parsedVersion.Symbol != "" {
		t.Fatal("Symbol should be empty")
	}

	if parsedVersion.Major != 1 {
		t.Fatal("Major should be 1")
	}

	if parsedVersion.Minor != 1 {
		t.Fatal("Minor should be 1")
	}

	if parsedVersion.Bug != 1 {
		t.Fatal("Bug should be 1")
	}

	if parsedVersion.Other != "" {
		t.Fatal("Other should be empty")
	}
}

func TestPrefix(t *testing.T) {
	parsedVersion, err := NpmStringToVersion("^1.1.1")

	if err != nil {
		t.Fatal(err)
	}

	if parsedVersion.Symbol != "^" {
		t.Fatal("Major should be ^")
	}

	if parsedVersion.Major != 1 {
		t.Fatal("Major should be 1")
	}

	if parsedVersion.Minor != 1 {
		t.Fatal("Minor should be 1")
	}

	if parsedVersion.Bug != 1 {
		t.Fatal("Bug should be 1")
	}

	if parsedVersion.Other != "" {
		t.Fatal("Other should be empty")
	}
}

func TestOther(t *testing.T) {
	parsedVersion, err := NpmStringToVersion("=1.1.1-alpha1")

	if err != nil {
		t.Fatal(err)
	}

	if parsedVersion.Symbol != "=" {
		t.Fatal("Major should be =")
	}

	if parsedVersion.Major != 1 {
		t.Fatal("Major should be 1")
	}

	if parsedVersion.Minor != 1 {
		t.Fatal("Minor should be 1")
	}

	if parsedVersion.Bug != 1 {
		t.Fatal("Bug should be 1")
	}

	if parsedVersion.Other != "alpha1" {
		t.Fatal("Other should be alpha1")
	}
}

func TestOtherWithoutDash(t *testing.T) {
	parsedVersion, err := NpmStringToVersion("=1.1.1rc0")

	if err != nil {
		t.Fatal(err)
	}

	if parsedVersion.Symbol != "=" {
		t.Fatal("Major should be =")
	}

	if parsedVersion.Major != 1 {
		t.Fatal("Major should be 1")
	}

	if parsedVersion.Minor != 1 {
		t.Fatal("Minor should be 1")
	}

	if parsedVersion.Bug != 1 {
		t.Fatal("Bug should be 1")
	}

	if parsedVersion.Other != "rc0" {
		t.Fatal("Other should be rc0")
	}
}

func TestWithNewLine(t *testing.T) {
	parsedVersion, err := NpmStringToVersion("1.0.2\n")

	if err != nil {
		t.Fatal(err)
	}

	if parsedVersion.Symbol != "" {
		t.Fatal("Major should be empty")
	}

	if parsedVersion.Major != 1 {
		t.Fatal("Major should be 1")
	}

	if parsedVersion.Minor != 0 {
		t.Fatal("Minor should be 0")
	}

	if parsedVersion.Bug != 2 {
		t.Fatal("Bug should be 2")
	}

	if parsedVersion.Other != "" {
		t.Fatal("Other should be empty")
	}
}

func TestError(t *testing.T) {
	_, err := NpmStringToVersion("")

	if err == nil {
		t.Fatal("Err should be filled")
	}
}