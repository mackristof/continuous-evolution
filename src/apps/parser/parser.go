package main

import (
	"encoding/json"
	"log"

	"continuous-evolution/src/helper"
	"continuous-evolution/src/queue"
	"continuous-evolution/src/model"
	"flag"
)

func parseDepth(property string, dat map[string]interface{}) ([]model.Package, error) {
	var ret []model.Package
	if dat[property] != nil {
		for k, v := range dat[property].(map[string]interface{}) {
			pkgVersion := v.(string)
			version, err := helper.NpmStringToVersion(pkgVersion)
			if err != nil {
				return make([]model.Package, 0), err
			}
			ret = append(ret, model.Package{
				Name: k,
				PkgVersion: pkgVersion,
				ParsedVersion: version,
			})
		}
	}
	return ret, nil
}

func parsePackageJson(project model.Project) ([]model.Package, error) {
	var ret []model.Package
	var dat map[string]interface{}
	if err := json.Unmarshal(project.OriginalFile, &dat); err != nil {
		return ret, err
	}
	dependencies, err := parseDepth("dependencies", dat)
	if err != nil {
		return ret, err
	}
	devDependencies, err := parseDepth("devDependencies", dat)
	if err != nil {
		return ret, err
	}
	peerDependencies, err := parseDepth("peerDependencies", dat)
	if err != nil {
		return ret, err
	}
	ret = append(ret, dependencies...)
	ret = append(ret, devDependencies...)
	ret = append(ret, peerDependencies...)
	return ret, nil
}

func main() {
	queueIp := flag.String("queue-ip", "127.0.0.1:4160", "The ip:port of the distributed queue")
	flag.Parse()

	s, _ := queue.NewFacade(*queueIp, helper.QUEUE_INPUT_PARSER, helper.QUEUE_INPUT_TOPIC)
	s.HandleProject(func (project model.Project) error {
		packages, err := parsePackageJson(project)
		if err != nil {
			log.Panic(err)
			return err
		}

		return s.PublishProject(helper.QUEUE_INPUT_RETRIEVER, project.AddPackages(packages))
	})
	s.Start(true)
}
