package main

import (
	"os/exec"
	"fmt"
	"log"
	"flag"

	"continuous-evolution/src/helper"
	"continuous-evolution/src/model"
	"continuous-evolution/src/queue"
)

func latestNpm(pkg model.Package, c chan model.Package) {
	cmd := exec.Command("npm", "show", pkg.Name, "version")
	ret, err := cmd.Output()
	if err != nil {
		fmt.Printf("Error on npm show %s : %s\n", pkg.Name, err)
		return;
	} else {
		latest, err := helper.NpmStringToVersion(string(ret))
		if err != nil {
			fmt.Printf("Error on parsing version %s : %s\n", pkg.Name, err)
			return;
		}
		c <- model.Package {
			Name: pkg.Name,
			PkgVersion: pkg.PkgVersion,
			ParsedVersion: latest,
		}
	}
}

func latestNpmVersions(pkgs []model.Package) ([]model.Package, error) {
	nbPkg := len(pkgs)
	c := make(chan model.Package, nbPkg)
	versions := make([]model.Package, 0)
	for _, pkg := range pkgs {
		go latestNpm(pkg, c)
	}
	for i := 0; i < nbPkg; i++ {
		versions = append(versions, <-c)
	}
	return versions, nil
}

func main() {
	queueIp := flag.String("queue-ip", "127.0.0.1:4160", "The ip:port of the distributed queue")
	flag.Parse()

	s, _ := queue.NewFacade(*queueIp, helper.QUEUE_INPUT_RETRIEVER, helper.QUEUE_INPUT_TOPIC)
	s.HandleProject(func (project model.Project) error {
		packages, err := latestNpmVersions(project.Packages)
		if err != nil {
			log.Panic(err)
			return err
		}

		return s.PublishProject(helper.QUEUE_INPUT_MAIN, project.AddLatestPackages(packages))
	})
	s.Start(true)
}