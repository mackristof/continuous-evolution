package main

import (
	"net/http"
	"io"
	"encoding/json"
	"flag"
	"fmt"

	"continuous-evolution/src/model"
	"continuous-evolution/src/helper"
	"continuous-evolution/src/queue"
	"log"
)

type message struct {
	Title string `json:"title"`
	Description string `json:"description"`
	AddProject string `json:"addProject"`
}

var URL_ADD_PROJECT string = "addProject"

func handler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	data, _ := json.Marshal(message {
		Title: "Continuous Evolution",
		Description: "Keep the world updated!",
		AddProject: fmt.Sprintf("%s/%s", r.Host, URL_ADD_PROJECT),
	})
	io.WriteString(w, string(data))
}

func handlerAddProject(queue queue.Queue) func (w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Content-Type", "application/json")
		query := r.URL.Query()
		if query.Get("project") != "" && query.Get("name") != "" {
			log.Println("Start", query.Get("project"))
			queue.PublishProject(helper.QUEUE_INPUT_DOWNLOADER, model.Project {
				Name: query.Get("name"),
				GitUrl: query.Get("project"),
				DepthManager: "npm",
				PathToWrite: "/tmp",
			})
		} else {
			data, _ := json.Marshal(message{
				Title: "Add new prject",
				Description: fmt.Sprintf("Add query %s/%s?project=gitUrl&name=projectName then look in your console ;)", r.Host, URL_ADD_PROJECT),
				AddProject: fmt.Sprintf("%s/%s", r.Host, URL_ADD_PROJECT),
			})
			io.WriteString(w, string(data))
		}
	}
}

func main() {
	queueIp := flag.String("queue-ip", "127.0.0.1:4150", "The ip:port of the distributed queue")
	flag.Parse()
	s, _ := queue.NewFacade(*queueIp, helper.QUEUE_INPUT_MAIN, "main")

	s.HandleProject(func (project model.Project) error {
		fmt.Printf("New versions for %s !!\n", project.Name)

		for _, p := range project.Packages {
			for _, lp := range project.LatestPackages {
				if p.Name == lp.Name && !p.ParsedVersion.Equal(lp.ParsedVersion) {
					fmt.Printf("You need to upgrade %s from %s to %s\n", p.Name, p.PkgVersion, lp.ParsedVersion)
				}
			}
		}

		return nil
	})
	s.Start(false)

	http.HandleFunc("/", handler)
	http.HandleFunc(fmt.Sprintf("/%s", URL_ADD_PROJECT), handlerAddProject(s))
	http.ListenAndServe(":1234", nil)
}