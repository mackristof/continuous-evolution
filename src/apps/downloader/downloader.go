package main

import (
	"os"
	"os/exec"
	"fmt"
	"log"

	"continuous-evolution/src/queue"
	"continuous-evolution/src/helper"
	"continuous-evolution/src/model"
	"flag"
	"path/filepath"
	"io/ioutil"
)

func gitClone(destDir string, projectName string, url string) error {
	//rm old dir if present
	if err := exec.Command("rm", "-rf", fmt.Sprintf("%s/%s", destDir, projectName)).Run(); err != nil {
		fmt.Printf("Error making rm -rf on %s/%s : %s\n", destDir, projectName, err)
	}

	currentDir, _ := os.Getwd()
	os.Chdir(destDir)
	cmd := exec.Command("git", "clone", "--depth=1", url)
	_, err := cmd.Output()
	os.Chdir(currentDir)
	return err
}

func main() {

	queueIp := flag.String("queue-ip", "127.0.0.1:4150", "The ip:port of the distributed queue")
	flag.Parse()

	s, _ := queue.NewFacade(*queueIp, helper.QUEUE_INPUT_DOWNLOADER, helper.QUEUE_INPUT_TOPIC)
	s.HandleProject(func (project model.Project) error {
		log.Println(project)
		if err := gitClone(project.PathToWrite, project.Name, project.GitUrl); err != nil {
			log.Panic(err)
			return err
		}

		visit := func (path string, f os.FileInfo, err error) error {
			if err == nil && f.Name() == "package.json" && f.IsDir() == false {
				data, err := ioutil.ReadFile(path)
				if err != nil {
					return err
				}
				return s.PublishProject(helper.QUEUE_INPUT_PARSER, project.AddOriginalFile(data))
			}
			return nil
		}

		if err := filepath.Walk(fmt.Sprintf("%s/%s", project.PathToWrite, project.Name), visit); err != nil {
			log.Panic(err)
			return err
		}

		return nil
	})
	s.Start(true)
}