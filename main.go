package main

import (
	"fmt"
	"continuous-evolution/src/helper"
	"continuous-evolution/src/queue"
	"continuous-evolution/src/model"
)

func main() {
	s, _ := queue.NewFacade("127.0.0.1:4150", helper.QUEUE_INPUT_MAIN, "main")

	s.HandleProject(func (project model.Project) error {
		fmt.Printf("New versions for %s !!\n", project.Name)

		for _, p := range project.Packages {
			for _, lp := range project.LatestPackages {
				if p.Name == lp.Name && !p.ParsedVersion.Equal(lp.ParsedVersion) {
					fmt.Printf("You need to upgrade %s from %s to %s\n", p.Name, p.PkgVersion, lp.ParsedVersion)
				}
			}
		}

		return nil
	})

	s.PublishProject(helper.QUEUE_INPUT_DOWNLOADER, model.Project {
		Name: "draw-my-project",
		GitUrl: "https://github.com/manland/draw-my-project.git",
		DepthManager: "npm",
		PathToWrite: "/tmp",
	})

	s.Start(true)

}
