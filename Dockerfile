FROM golang

RUN apt-get update && \
    apt-get install -y --no-install-recommends git && \
    curl -sL https://deb.nodesource.com/setup_7.x | bash - && \
    apt-get install -y nodejs

ADD ./ $GOPATH/src/continuous-evolution/

WORKDIR $GOPATH/src/continuous-evolution/

RUN sh install.sh && \
    sh build.sh && \
    ln -s $GOPATH/src/continuous-evolution/build/* /bin