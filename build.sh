#!/bin/bash

rm -rf build
cd src/apps
find . -maxdepth 1 -type d \
    \( ! -name . \) \
    -exec bash -c "cd '{}' && go build -race -ldflags \"-extldflags '-static'\" -o ../../../build/{}" \;